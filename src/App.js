import React from 'react';
import './App.css';
import Login from './Components/Login';
import FamilyTreeTable from './Components/FamilyTreeTable'
import NavBar from './Components/NavBar'
import Register from './Components/Register';
import EditAccount from './Components/EditAccount'
import EditMember from './Components/EditMember'
import ChangePass from './Components/ChangePass'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';


function App() {

  return (
    <div className="App">
      <Router>
      <NavBar />
      <h1>Family Pedigree</h1><br></br><br></br>
        <Switch>
          <Route path="/" exact component={FamilyTreeTable} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/editaccount" component={EditAccount} />
          <Route path="/editmember/:id" component={EditMember} />
          <Route path="/changepassword" component={ChangePass} />
        </Switch>
      </Router>
    </div>
  );
}

export default App