export const saveAccessToken = (accessToken) => {
    return{
        type: 'saveAccessToken',
        accessToken: accessToken
    }
}

export const revokeAccessToken = () => {
    return{
        type: 'revokeAccessToken'
    }
}

export const login = () => {
    return{
        type: 'login'
    }
}

export const logout = () => {
    return{
        type: 'logout'
    }
}

export const setLoggedInUser = (loggedInUser) => {
    return{
        type: 'setLoggedInUser',
        loggedInUser: loggedInUser
    }
}

export const setLoggedInUserEmpty = () => {
    return{
        type: 'setLoggedInUserEmpty'
    }
}

export const setFamilyArray = (familyArray) => {
    return{
        type: 'setFamilyArray',
        familyArray: familyArray
    }
}

export const setFamilyArrayEmpty = () => {
    return{
        type: 'setFamilyArrayEmpty'
    }
}

export const pushToFamilyArray = (newMember) => {
    return{
        type: 'pushToFamilyArray',
        newMember: newMember
    }
}