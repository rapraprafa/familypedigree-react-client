import React from 'react';
import getTokenData from '../constants';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { saveAccessToken, login, setLoggedInUser, setFamilyArray } from '../redux_actions/index';

const axios = require('axios');
const oauth = require('axios-oauth-client');



class Register extends React.Component {
    constructor() {
        super()
        this.state = {
            firstname: "",
            lastname: "",
            username: "",
            birthday: "",
            email: "",
            password1: "",
            password2: "",
            message: "",
            isValid: true
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        const getOwnerCredentials = oauth.client(axios.create(), {
            url: 'http://localhost:8000/o/token/',
            grant_type: 'password',
            client_id: getTokenData.client_id,
            client_secret: getTokenData.client_secret,
            username: getTokenData.username,
            password: getTokenData.password
        });
        if(this.state.password1 !== this.state.password2){
            this.setState({
                message: "Passwords do not match."
            })
            return;
        }
        const registerUser = async () => {
            const auth = await getOwnerCredentials();
            this.props.saveAccessToken(auth.access_token);
            const newUser = {
                username: this.state.username,
                first_name: this.state.firstname,
                last_name: this.state.lastname,
                email: this.state.email,
                password: this.state.password1
            }
            const headers = {
                headers: { Authorization: `Bearer ${auth.access_token}` }
            };
            axios.post(`http://localhost:8000/api/users/`, newUser, headers)
                .then(res => {
                    this.props.setLoggedInUser(res.data);
                    this.props.login();
                    this.props.saveAccessToken(auth.access_token);
                    const userDataToFamily = {
                        familylistowner_id: res.data.id,
                        relations:'Me',
                        firstnames: res.data.first_name,
                        lastnames: res.data.last_name,
                        birthdays: this.state.birthday,
                        vitalstatuses: "Alive"
                    }
                    axios.post(`http://localhost:8000/api/family/`, userDataToFamily, headers)
                    .then(res => {
                        var familyarray = [];
                        familyarray.push({
                            Id: 0,
                            relation: res.data.relations,
                            firstname: res.data.firstnames,
                            lastname: res.data.lastnames,
                            birthday: res.data.birthdays,
                            vitalstatus: res.data.vitalstatuses
                        });
                        this.props.setFamilyArray(familyarray);
                    })
                    .catch(error => {
                        if(error.response){
                            console.log(error.response.data);
                            this.setState({
                                message: error.response.data,
                                isValid: false
                            })
                        }
                    })
                    if(this.state.isValid === false){
                        return;
                    }
                })
                .catch(error => {
                    if(error.response){
                        console.log(error.response.data);
                        this.setState({
                            message: "Username or email already exists.",
                            isValid: false
                        })
                    }
                })
                if (this.state.isValid === false) {
                    return;
                }
        };
        registerUser();
    }

    render() {
        if (this.props.isLoggedIn) {
            return <Redirect to="/" />
        }
        else {
            return (
                <div>
                    <h1>Register</h1>
                    <form onSubmit={this.handleSubmit}>
                        <input type="text" name="firstname" placeholder="First Name" onChange={this.handleChange} /><br></br><br></br>
                        <input type="text" name="lastname" placeholder="Last Name" onChange={this.handleChange} /><br></br><br></br>
                        <input type="text" name="username" placeholder="Username" onChange={this.handleChange} /><br></br><br></br>
                        <label htmlFor="birthday">Birthday:</label>
                        <input type="date" id="birthday" name="birthday" onChange={this.handleChange} /><br></br><br></br>
                        <input type="email" name="email" placeholder="E-mail" onChange={this.handleChange} /><br></br><br></br>
                        <input type="password" name="password1" placeholder="Password" onChange={this.handleChange} /><br></br><br></br>
                        <input type="password" name="password2" placeholder="Confirm Password" onChange={this.handleChange} /><br></br><br></br>
                        <input type="submit" value="Register" className="btn btn-dark"/>
                        <h3>{this.state.message}</h3>
                    </form>
                </div>
            )
        }
    }

}

const mapStateToProps = (state) => {
    return {
        accessToken: state.accessToken,
        isLoggedIn: state.isLoggedIn,
        loggedInUser: state.loggedInUser
    }
}

const mapDispatchToProps = () => {
    return {
        saveAccessToken,
        login,
        setLoggedInUser,
        setFamilyArray
    }
}


export default connect(mapStateToProps, mapDispatchToProps())(Register);