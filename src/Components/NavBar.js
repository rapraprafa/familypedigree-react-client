import React from 'react';
import './NavBar.css';
import { revokeAccessToken, logout, setLoggedInUserEmpty, setFamilyArrayEmpty } from '../redux_actions/index'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux';
import getTokenData from '../constants'
const axios = require('axios');
const oauth = require('axios-oauth-client');

function NavBar() {

    const isLoggedIn = useSelector(state => state.isLoggedIn);
    const loggedInUser = useSelector(state => state.loggedInUser);
    const accessToken = useSelector(state => state.accessToken);
    const dispatch = useDispatch();

    function handleLogOut(event) {
        const revokeToken = oauth.client(axios.create(), {
            url: 'http://localhost:8000/o/revoke_token/',
            token: accessToken,
            client_id: getTokenData.client_id,
            client_secret: getTokenData.client_secret
        });
        const logOutUser = async () => {
            const revoked = await revokeToken()
            console.log(revoked);
            dispatch(revokeAccessToken())
            dispatch(logout())
            dispatch(setLoggedInUserEmpty())
            dispatch(setFamilyArrayEmpty())
        }
        logOutUser();
    }


    return (
        <div className="topnav">
            <ul>
                <Link to="/">
                    <li className="active">Home</li>
                </Link>

                {isLoggedIn ?
                    <div>
                        <Link to="/login">
                            <li onClick={() => handleLogOut()}>Logout</li>
                        </Link>
                        <li>Hello, {loggedInUser.first_name}</li>
                        <Link to="/editaccount">
                            <li>Edit User Details</li>
                        </Link>
                    </div>
                    :
                    <div>
                        <Link to="/register">
                            <li>Register</li>
                        </Link>
                        <Link to="/login">
                            <li>Login</li>
                        </Link>
                    </div>}
            </ul>
        </div>
    )
}


export default NavBar;