import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { setLoggedInUser } from '../redux_actions/index';
const axios = require('axios');

class ChangePass extends React.Component {
    constructor() {
        super()
        this.state = {
            password1: "",
            password2: "",
            message: "",
            success: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        if(this.state.password1===this.state.password2){
            const userPut = {
                id: this.props.loggedInUser.id,
                password: this.state.password1
            }
            const headers = {
                headers: { Authorization: `Bearer ${this.props.accessToken}` }
            };
            axios.put('http://localhost:8000/api/users/' + this.props.loggedInUser.id + '/', userPut, headers)
            .then(res => {
                const updatedUser = res.data
                this.props.setLoggedInUser(updatedUser);
                this.setState({
                    success: true
                })
            })
            .catch(error => {
                if(error.response){
                    console.log(error.response)
                }
            })
        }
        else{
            this.setState({
                message: "Passwords do not match."
            })
        }
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        if (this.state.success){
            return <Redirect to="/editaccount" />
        }
        if (this.props.isLoggedIn === false) {
            return <Redirect to="/login" />
        }
        else {
            return (
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <input type="password" name="password1" onChange={this.handleChange} placeholder="New Password" /><br /><br />
                        <input type="password" name="password2" onChange={this.handleChange} placeholder="Confirm New Password" /><br /><br />
                        <input type="submit" value="Change password" className="btn btn-dark"/>
                        <h3>{this.state.message}</h3>
                    </form>
                </div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.accessToken,
        isLoggedIn: state.isLoggedIn,
        loggedInUser: state.loggedInUser,
        familyArray: state.familyArray
    }
}

const mapDispatchToProps = () => {
    return {
        setLoggedInUser
    }
}

export default connect(mapStateToProps, mapDispatchToProps())(ChangePass);