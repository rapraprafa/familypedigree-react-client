import React from 'react';
import { connect } from 'react-redux';
import { saveAccessToken, login, logout, setFamilyArray } from '../redux_actions/index';
import { Redirect, Link } from 'react-router-dom';
import AddMember from './AddMember';
const axios = require('axios');

class FamilyTreeTable extends React.Component {
    constructor() {
        super()
        this.state = {
        }
        this.handleDelete = this.handleDelete.bind(this)
    }

    handleDelete(index) {
        this.props.familyArray.splice(index, 1);
        var relationsupdated = ""
        var firstnamesupdated = ""
        var lastnamesupdated = ""
        var birthdaysupdated = ""
        var vitalstatusesupdated = ""
        for (let i = 0; i < this.props.familyArray.length; i++) {
            relationsupdated = relationsupdated + "," + this.props.familyArray[i].relation;
            firstnamesupdated = firstnamesupdated + "," + this.props.familyArray[i].firstname;
            lastnamesupdated = lastnamesupdated + "," + this.props.familyArray[i].lastname;
            birthdaysupdated = birthdaysupdated + "," + this.props.familyArray[i].birthday;
            vitalstatusesupdated = vitalstatusesupdated + "," + this.props.familyArray[i].vitalstatus;
        }
        relationsupdated = relationsupdated.slice(1);
        firstnamesupdated = firstnamesupdated.slice(1);
        lastnamesupdated = lastnamesupdated.slice(1);
        birthdaysupdated = birthdaysupdated.slice(1);
        vitalstatusesupdated = vitalstatusesupdated.slice(1);
        const familyPutData = {
            familylistowner_id: this.props.loggedInUser.id,
            relations: relationsupdated,
            firstnames: firstnamesupdated,
            lastnames: lastnamesupdated,
            birthdays: birthdaysupdated,
            vitalstatuses: vitalstatusesupdated
        }
        const headers = {
            headers: { Authorization: `Bearer ${this.props.accessToken}` }
        };
        axios.put('http://localhost:8000/api/family/' + this.props.loggedInUser.id, familyPutData, headers)
            .then(res => {
                const family = res.data;
                const relations = family.relations.split(',');
                const firstnames = family.firstnames.split(',');
                const lastnames = family.lastnames.split(',');
                const birthdays = family.birthdays.split(',');
                const vitalstatuses = family.vitalstatuses.split(',');
                var familyarray = []
                for (let i = 0; i < relations.length; i++) {
                    var member = {
                        Id: i,
                        relation: relations[i],
                        firstname: firstnames[i],
                        lastname: lastnames[i],
                        birthday: birthdays[i],
                        vitalstatus: vitalstatuses[i]
                    }
                    familyarray.push(member)
                }
                this.props.setFamilyArray(familyarray);
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            })
    }

    render() {
        if (this.props.isLoggedIn === false) {
            return <Redirect to="/login" />
        }
        else {
            return (
                <div className="container main-content">
                    <div className="row">
                        <div className="col">Relationship</div>
                        <div className="col">First name</div>
                        <div className="col">Last name</div>
                        <div className="col">Birthday</div>
                        <div className="col">Vital Status</div>
                        <div className="col">Actions</div>
                    </div>
                    <br></br><br></br><br></br>
                    {this.props.familyArray.map(member =>
                        <div key={member.Id} className="row">
                            <div className="col">
                                {member.relation}
                            </div>
                            <div className="col">
                                {member.firstname}
                            </div>
                            <div className="col">
                                {member.lastname}
                            </div>
                            <div className="col">
                                {member.birthday}
                            </div>
                            <div className="col">
                                {member.vitalstatus}
                            </div>
                            {member.Id === 0 ?
                                <div className="col">
                                    <Link to={`/editmember/${member.Id}`}><button className="btn btn-dark">Edit</button></Link>
                                </div> :
                                <div className="col">
                                    <Link to={`/editmember/${member.Id}`}><button className="btn btn-dark">Edit</button></Link>
                                    <button className="btn btn-danger" onClick={() => this.handleDelete(member.Id)}>Delete</button>
                                </div>
                            }
                            <br></br>
                            <br></br>
                            <br></br>
                        </div>)
                    }
                    <div>
                        <AddMember />
                    </div>
                </div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.accessToken,
        isLoggedIn: state.isLoggedIn,
        loggedInUser: state.loggedInUser,
        familyArray: state.familyArray
    }
}

const mapDispatchToProps = () => {
    return {
        saveAccessToken,
        login,
        logout,
        setFamilyArray
    }
}


export default connect(mapStateToProps, mapDispatchToProps())(FamilyTreeTable);