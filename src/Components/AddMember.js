import React from 'react';
import { connect } from 'react-redux';
import { setFamilyArray } from '../redux_actions/index';
const axios = require('axios');

class AddMember extends React.Component {
    constructor() {
        super()
        this.state = {
            relation: "",
            firstname: "",
            lastname: "",
            birthday: "",
            vitalstatus: ""
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        const headers = {
            headers: { Authorization: `Bearer ${this.props.accessToken}` }
        };
        axios.get('http://localhost:8000/api/family/'+this.props.loggedInUser.id, headers)
            .then(res => {
                var putData = {
                    familylistowner_id: res.data.familylistowner_id,
                    relations: res.data.relations + "," + this.state.relation,
                    firstnames: res.data.firstnames + "," + this.state.firstname,
                    lastnames: res.data.lastnames + "," + this.state.lastname,
                    birthdays: res.data.birthdays + "," + this.state.birthday,
                    vitalstatuses: res.data.vitalstatuses + "," + this.state.vitalstatus
                }
                axios.put('http://localhost:8000/api/family/' + this.props.loggedInUser.id, putData, headers)
                    .then(res => {
                        const family = res.data;
                        const relations = family.relations.split(',');
                        const firstnames = family.firstnames.split(',');
                        const lastnames = family.lastnames.split(',');
                        const birthdays = family.birthdays.split(',');
                        const vitalstatuses = family.vitalstatuses.split(',');
                        var familyarray = []
                        for (let i = 0; i < relations.length; i++) {
                            var member = {
                                Id: i,
                                relation: relations[i],
                                firstname: firstnames[i],
                                lastname: lastnames[i],
                                birthday: birthdays[i],
                                vitalstatus: vitalstatuses[i]
                            }
                            familyarray.push(member)
                        }
                        this.props.setFamilyArray(familyarray);
                        this.setState({
                            relation: "",
                            firstname: "",
                            lastname: "",
                            birthday: "",
                            vitalstatus: ""
                        })
                    })
                    .catch(error => {
                        if(error.response){
                            this.setState({message: error.response});
                        }
                    })
            })
    }

    render() {
        return (
            <div>
                <h2> Add a member </h2>
                <form onSubmit={this.handleSubmit}>
                    <input type="text" name="relation" value={this.state.relation} placeholder="Relationship" onChange={this.handleChange}/><br></br>
                    <input type="text" name="firstname" value={this.state.firstname} placeholder="First Name" onChange={this.handleChange}/><br></br>
                    <input type="text" name="lastname" value={this.state.lastname} placeholder="Last Name" onChange={this.handleChange}/><br></br>
                    <input type="date" name="birthday" value={this.state.birthday} placeholder="Birthday" onChange={this.handleChange}/><br></br>
                    <input type="text" name="vitalstatus" value={this.state.vitalstatus} placeholder="Vital status" onChange={this.handleChange}/><br></br>
                    <input type="submit" value="Add" className="btn btn-dark"/><br></br>
                </form>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        accessToken: state.accessToken,
        loggedInUser: state.loggedInUser,
        familyArray: state.familyArray
    }
}

const mapDispatchToProps = () => {
    return {
        setFamilyArray
    }
}


export default connect(mapStateToProps, mapDispatchToProps())(AddMember);