import React from 'react';
import { Redirect, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { saveAccessToken, login, setLoggedInUser, setFamilyArray } from '../redux_actions/index';
const axios = require('axios');


class EditAccount extends React.Component {
    constructor() {
        super()
        this.state = {
            firstname: "",
            lastname: "",
            email: "",
            successSave: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.setState({
            firstname: this.props.loggedInUser.first_name,
            lastname: this.props.loggedInUser.last_name,
            email: this.props.loggedInUser.email
        })
    }


    handleSubmit(event) {
        event.preventDefault();
        const headers = {
            headers: { Authorization: `Bearer ${this.props.accessToken}` }
        };
        const userPut = {
            id: this.props.loggedInUser.id,
            first_name: this.state.firstname,
            last_name: this.state.lastname,
            email: this.state.email
        }
        axios.put('http://localhost:8000/api/users/' + this.props.loggedInUser.id + '/', userPut, headers)
            .then(res => {
                const userUpdated = res.data;
                this.props.setLoggedInUser(userUpdated)
                this.props.familyArray[0].firstname = userUpdated.first_name
                this.props.familyArray[0].lastname = userUpdated.last_name
                var relationsupdated = ""
                var firstnamesupdated = ""
                var lastnamesupdated = ""
                var birthdaysupdated = ""
                var vitalstatusesupdated = ""
                for (let i = 0; i < this.props.familyArray.length; i++) {
                    relationsupdated = relationsupdated + "," + this.props.familyArray[i].relation
                    firstnamesupdated = firstnamesupdated + "," + this.props.familyArray[i].firstname
                    lastnamesupdated = lastnamesupdated + "," + this.props.familyArray[i].lastname
                    birthdaysupdated = birthdaysupdated + "," + this.props.familyArray[i].birthday
                    vitalstatusesupdated = vitalstatusesupdated + "," + this.props.familyArray[i].vitalstatus
                }
                relationsupdated = relationsupdated.slice(1);
                firstnamesupdated = firstnamesupdated.slice(1);
                lastnamesupdated = lastnamesupdated.slice(1);
                birthdaysupdated = birthdaysupdated.slice(1);
                vitalstatusesupdated = vitalstatusesupdated.slice(1);
                const familyPutData = {
                    familylistowner_id: this.props.loggedInUser.id,
                    relations: relationsupdated,
                    firstnames: firstnamesupdated,
                    lastnames: lastnamesupdated,
                    birthdays: birthdaysupdated,
                    vitalstatuses: vitalstatusesupdated
                }
                axios.put('http://localhost:8000/api/family/' + this.props.loggedInUser.id, familyPutData, headers)
                    .then(res => {
                        const family = res.data;
                        const relations = family.relations.split(',');
                        const firstnames = family.firstnames.split(',');
                        const lastnames = family.lastnames.split(',');
                        const birthdays = family.birthdays.split(',');
                        const vitalstatuses = family.vitalstatuses.split(',');
                        var familyarray = []
                        for (let i = 0; i < relations.length; i++) {
                            var member = {
                                Id: i,
                                relation: relations[i],
                                firstname: firstnames[i],
                                lastname: lastnames[i],
                                birthday: birthdays[i],
                                vitalstatus: vitalstatuses[i]
                            }
                            familyarray.push(member)
                        }
                        this.props.setFamilyArray(familyarray);
                        this.setState({
                            successSave: true
                        })
                    })
                    .catch(error => {
                        if (error.response) {
                            console.log(error.response);
                        }
                    })
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            })
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        if (this.state.successSave){
            return <Redirect to="/" />
        }
        if (this.props.isLoggedIn === false) {
            return <Redirect to="/login" />
        }
        else {
            return (
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <input type="text" name="firstname" placeholder="First name" onChange={this.handleChange} value={this.state.firstname} /><br /><br />
                        <input type="text" name="lastname" placeholder="Last name" onChange={this.handleChange} value={this.state.lastname} /><br /><br />
                        <input type="text" name="email" placeholder="E-mail" onChange={this.handleChange} value={this.state.email} /><br /><br />
                        <Link to="/changepassword"><button type="button" className="btn btn-info" name="changepass">Change password</button></Link><br /><br />
                        <input type="submit" value="Save" className="btn btn-dark"/>
                    </form>
                </div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.accessToken,
        isLoggedIn: state.isLoggedIn,
        loggedInUser: state.loggedInUser,
        familyArray: state.familyArray
    }
}

const mapDispatchToProps = () => {
    return {
        saveAccessToken,
        login,
        setLoggedInUser,
        setFamilyArray
    }
}

export default connect(mapStateToProps, mapDispatchToProps())(EditAccount);