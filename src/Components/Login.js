import React from 'react';
import getTokenData from '../constants';
import { connect } from 'react-redux';
import { saveAccessToken, login, setLoggedInUser, setFamilyArray } from '../redux_actions/index';
import { Redirect } from 'react-router-dom';
const axios = require('axios');
const oauth = require('axios-oauth-client');



class Login extends React.Component {
    constructor() {
        super()
        this.state = {
            username: "",
            password: "",
            isValidUser: true,
            user: {}
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit(event) {
        this.setState({ isValidUser: true });
        event.preventDefault()
        const getOwnerCredentials = oauth.client(axios.create(), {
            url: 'http://localhost:8000/o/token/',
            grant_type: 'password',
            client_id: getTokenData.client_id,
            client_secret: getTokenData.client_secret,
            username: this.state.username,
            password: this.state.password
        });
        const loginUser = async () => {
            const auth = await getOwnerCredentials()
                .catch(error => {
                    if (error.response) {
                        console.log(error.response.data);
                        this.setState({ isValidUser: false });
                    }
                })
            if (this.state.isValidUser === false) {
                return;
            }
            const user = {
                username: this.state.username,
                password: this.state.password
            };
            const headers = {
                headers: { Authorization: `Bearer ${auth.access_token}` }
            };
            axios.post(`http://localhost:8000/api/login/`, user, headers)
                .then(res => {
                    this.props.setLoggedInUser(res.data);
                    this.props.login();
                    this.props.saveAccessToken(auth.access_token);
                    axios.get('http://localhost:8000/api/family/' + res.data.id, headers)
                        .then(res => {
                            const family = res.data;
                            const relations = family.relations.split(',');
                            const firstnames = family.firstnames.split(',');
                            const lastnames = family.lastnames.split(',');
                            const birthdays = family.birthdays.split(',');
                            const vitalstatuses = family.vitalstatuses.split(',');
                            var familyarray = []
                            for (let i = 0; i < relations.length; i++) {
                                var member = {
                                    Id: i,
                                    relation: relations[i],
                                    firstname: firstnames[i],
                                    lastname: lastnames[i],
                                    birthday: birthdays[i],
                                    vitalstatus: vitalstatuses[i]
                                }
                                familyarray.push(member)
                            }
                            this.props.setFamilyArray(familyarray);
                        })
                        .catch(error => {
                            if (error.response) {
                                console.log(error.response.data);
                            }
                        })
                })
                .catch(error => {
                    if (error.response) {
                        console.log(error.response.data);
                        this.setState({ isValidUser: false });
                    }
                })
            if (this.state.isValidUser === false) {
                return;
            }
        };
        loginUser();
    }

    render() {
        if (this.props.isLoggedIn) {
            return <Redirect to="/" />
        }
        else {
            const validText = this.state.isValidUser ? "" : "Invalid Credentials"
            return (
                <div>
                    <h1>Login</h1>
                    <form onSubmit={this.handleSubmit}>
                        <input type="text" name="username" placeholder="Username" onChange={this.handleChange} /><br></br><br></br>
                        <input type="password" name="password" placeholder="Password" onChange={this.handleChange} /><br></br><br></br>
                        <input type="submit" value="Login" className="btn btn-dark"/>
                        <h3>{validText}</h3>
                    </form>
                </div>
            )
        }

    }

}

const mapStateToProps = (state) => {
    return {
        accessToken: state.accessToken,
        isLoggedIn: state.isLoggedIn,
        loggedInUser: state.loggedInUser,
        familyArray: state.familyArray
    }
}

const mapDispatchToProps = () => {
    return {
        saveAccessToken,
        login,
        setLoggedInUser,
        setFamilyArray
    }
}


export default connect(mapStateToProps, mapDispatchToProps())(Login);