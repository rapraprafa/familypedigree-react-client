import React from 'react';
import { saveAccessToken, login, logout, setFamilyArray, setLoggedInUser } from '../redux_actions/index';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
const axios = require('axios');


class EditMember extends React.Component {
    constructor(match) {
        super()
        this.state = {
            relation: "",
            firstname: "",
            lastname: "",
            birthday: "",
            vitalstatus: "",
            success: false,
            match: match
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.setState({
            relation: this.props.familyArray[this.state.match.match.params.id].relation,
            firstname: this.props.familyArray[this.state.match.match.params.id].firstname,
            lastname: this.props.familyArray[this.state.match.match.params.id].lastname,
            birthday: this.props.familyArray[this.state.match.match.params.id].birthday,
            vitalstatus: this.props.familyArray[this.state.match.match.params.id].vitalstatus
        })
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        const headers = {
            headers: { Authorization: `Bearer ${this.props.accessToken}` }
        };
        this.props.familyArray[this.state.match.match.params.id].relation = this.state.relation
        this.props.familyArray[this.state.match.match.params.id].firstname = this.state.firstname
        this.props.familyArray[this.state.match.match.params.id].lastname = this.state.lastname
        this.props.familyArray[this.state.match.match.params.id].birthday = this.state.birthday
        this.props.familyArray[this.state.match.match.params.id].vitalstatus = this.state.vitalstatus
        var relationsupdated = "";
        var firstnamesupdated = "";
        var lastnamesupdated = "";
        var birthdaysupdated = "";
        var vitalstatusesupdated = "";
        for (let i = 0; i < this.props.familyArray.length; i++) {
            relationsupdated = relationsupdated + "," + this.props.familyArray[i].relation;
            firstnamesupdated = firstnamesupdated + "," + this.props.familyArray[i].firstname;
            lastnamesupdated = lastnamesupdated + "," + this.props.familyArray[i].lastname;
            birthdaysupdated = birthdaysupdated + "," + this.props.familyArray[i].birthday;
            vitalstatusesupdated = vitalstatusesupdated + "," + this.props.familyArray[i].vitalstatus;
        }
        relationsupdated = relationsupdated.slice(1);
        firstnamesupdated = firstnamesupdated.slice(1);
        lastnamesupdated = lastnamesupdated.slice(1);
        birthdaysupdated = birthdaysupdated.slice(1);
        vitalstatusesupdated = vitalstatusesupdated.slice(1);
        const familyPutData = {
            familylistowner_id: this.props.loggedInUser.id,
            relations: relationsupdated,
            firstnames: firstnamesupdated,
            lastnames: lastnamesupdated,
            birthdays: birthdaysupdated,
            vitalstatuses: vitalstatusesupdated
        }
        axios.put('http://localhost:8000/api/family/' + this.props.loggedInUser.id, familyPutData, headers)
            .then(res => {
                const family = res.data;
                const relations = family.relations.split(',');
                const firstnames = family.firstnames.split(',');
                const lastnames = family.lastnames.split(',');
                const birthdays = family.birthdays.split(',');
                const vitalstatuses = family.vitalstatuses.split(',');
                var familyarray = []
                for (let i = 0; i < relations.length; i++) {
                    var member = {
                        Id: i,
                        relation: relations[i],
                        firstname: firstnames[i],
                        lastname: lastnames[i],
                        birthday: birthdays[i],
                        vitalstatus: vitalstatuses[i]
                    }
                    familyarray.push(member)
                }
                this.props.setFamilyArray(familyarray);
                this.setState({
                    success: true
                })
                const userPut = {
                    id: this.props.loggedInUser.id,
                    first_name: this.state.firstname,
                    last_name: this.state.lastname
                }
                if (this.state.match.match.params.id === 0 || this.state.match.match.params.id === "0") {
                    axios.put('http://localhost:8000/api/users/' + this.props.loggedInUser.id + '/', userPut, headers)
                        .then(res => {
                            const userUpdated = res.data;
                            this.props.setLoggedInUser(userUpdated)
                            this.setState({
                                success: true
                            })
                        })
                        .catch(error => {
                            if (error.response) {
                                console.log(error.response);
                            }
                        })
                }
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            })
    }

    render() {
        if (this.state.success === true) {
            return <Redirect to="/" />
        }
        if (this.props.isLoggedIn === false) {
            return <Redirect to="/login" />
        }
        else {
            return (
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <input type="text" name="relation" value={this.state.relation} placeholder="Relationship" onChange={this.handleChange} /><br />
                        <input type="text" name="firstname" value={this.state.firstname} placeholder="First Name" onChange={this.handleChange} /><br />
                        <input type="text" name="lastname" value={this.state.lastname} placeholder="Last Name" onChange={this.handleChange} /><br />
                        <input type="date" name="birthday" placeholder="Birthday" value={this.state.birthday} onChange={this.handleChange} /><br />
                        <input type="text" name="vitalstatus" value={this.state.vitalstatus} placeholder="Vital status" onChange={this.handleChange} /><br />
                        <input type="submit" value="Save" className="btn btn-dark" />
                    </form>
                </div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.accessToken,
        isLoggedIn: state.isLoggedIn,
        loggedInUser: state.loggedInUser,
        familyArray: state.familyArray
    }
}

const mapDispatchToProps = () => {
    return {
        saveAccessToken,
        login,
        logout,
        setFamilyArray,
        setLoggedInUser
    }
}


export default connect(mapStateToProps, mapDispatchToProps())(EditMember);