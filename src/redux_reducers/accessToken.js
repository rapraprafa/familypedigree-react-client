const accessTokenReducer = (state = '', action) => {
    switch(action.type){
        case 'saveAccessToken':
            return action.accessToken;
        case 'revokeAccessToken':
            return '';
        default:
            return state;
    }
};

export default accessTokenReducer;