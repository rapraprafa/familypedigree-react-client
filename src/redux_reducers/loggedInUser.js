const loggedInUserReducer = (state = {}, action) => {
    switch(action.type){
        case 'setLoggedInUser':
            return action.loggedInUser;
        case 'setLoggedInUserEmpty':
            return {};
        default:
            return state;
    }
};

export default loggedInUserReducer;