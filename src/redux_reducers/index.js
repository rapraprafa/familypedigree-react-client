import { combineReducers } from 'redux';
import accessTokenReducer from './accessToken';
import isLoggedInReducer from './isLoggedIn';
import loggedInUserReducer from './loggedInUser';
import familyArrayReducer from './familyArrayReducer';

const allReducers = combineReducers({
    accessToken: accessTokenReducer,
    isLoggedIn: isLoggedInReducer,
    loggedInUser: loggedInUserReducer,
    familyArray: familyArrayReducer
})

export default allReducers;