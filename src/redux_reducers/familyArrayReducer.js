const familyArrayReducer = (state = [{}], action) => {
    switch(action.type){
        case 'setFamilyArray':
            return action.familyArray;
        case 'setFamilyArrayEmpty':
            return [{}];
        case 'pushToFamilyArray':
            const newFamilyArray = state.push(action.newMember);
            return newFamilyArray;
        default:
            return state;
    }
};

export default familyArrayReducer;